package com.biblezon.tapIt.utils;

public interface OnRearrangeListener {
	
	public abstract void onRearrange(int oldIndex, int newIndex);
}
