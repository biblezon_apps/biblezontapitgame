package com.biblezon.tapIt.webservice;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.graphics.Bitmap;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.biblezon.tapIt.application.AppApplicationController;
import com.biblezon.tapIt.ihelper.GlobalKeys;
import com.biblezon.tapIt.utils.AppDefaultUtils;
import com.biblezon.tapIt.utils.AppDialogUtils;
import com.biblezon.tapIt.utils.AppHelper;
import com.biblezon.tapIt.webservice.control.WebAPIResponseListener;
import com.biblezon.tapIt.webservice.control.WebserviceResponseHandler;

/**
 * get activity list Handler
 *
 * @author Anshuman
 */
public class MemoryMatchAPIHandler {
    /**
     * Instance object of activity API
     */
    private Activity mActivity;
    /**
     * Debug TAG
     */
    private String TAG = MemoryMatchAPIHandler.class.getSimpleName();
    private String key = "MEMORY_MATCHES_KEY";

    private ArrayList<String> imageArrays = new ArrayList<String>();
    private ArrayList<String> mBitmapPathArray = new ArrayList<String>();
    private int dataCount = 0;

    /**
     * @param mActivity
     * @param webAPIResponseListener
     * @param MassUrl
     */
    public MemoryMatchAPIHandler(Activity mActivity,
                                 WebAPIResponseListener webAPIResponseListener) {
        this.mActivity = mActivity;
        postAPICall();

    }

    /**
     * Making json object request
     */
    public void postAPICall() {
        /**
         * JSON Request
         */
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.GET,
                (GlobalKeys.BASE_URL + GlobalKeys.MEMORYMATCHES).trim(), "",
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        AppDefaultUtils.showInfoLog(TAG, "Response :"
                                + response);
                        parseAPIResponse(response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {

        };

        // Adding request to request queue
        AppApplicationController.getInstance().addToRequestQueue(jsonObjReq,
                key);
        // set request time-out
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                AppHelper.ONE_SECOND_TIME * AppHelper.API_REQUEST_TIME,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Canceling request
        // MassAppApplicationController.getInstance().getRequestQueue()
        // .cancelAll(GlobalKeys.MASS_ACTIVITY_API);
    }

    /**
     * Parse Trip History API Response
     *
     * @param response
     */
    protected void parseAPIResponse(JSONObject response) {
        imageArrays = new ArrayList<String>();
        mBitmapPathArray = new ArrayList<String>();
        if (WebserviceResponseHandler.getInstance().checkResponseCode(response)) {
            /* Success of API Response */
            try {
                JSONArray mArrayData = response.getJSONArray(GlobalKeys.DATA);
                dataCount = mArrayData.length();
                dataCount = dataCount * 9;
                for (int i = 0; i < mArrayData.length(); i++) {
                    JSONObject mJsonObject = mArrayData.getJSONObject(i);
                    if (mJsonObject != null) {
                        if (mJsonObject.has(GlobalKeys.IMAGE_DEFAULT)) {
                            String image_url = mJsonObject
                                    .getString(GlobalKeys.IMAGE_DEFAULT);
                            String nonIntegerString = image_url.replaceAll(
                                    "[0-9]", "");
                            String sub = nonIntegerString.substring(
                                    nonIntegerString.lastIndexOf("/") + 1,
                                    nonIntegerString.lastIndexOf("."));
                            imageArrays.add(sub);
                            String filenameWithExt = sub + ".png";
                            if (!AppDefaultUtils.isFilePresent(filenameWithExt)) {
                                new GetUserImageAPIHandler(mActivity,
                                        image_url, downloadImageResponse(sub),
                                        false, i);
                            } else {
                                AppDefaultUtils.showLog(TAG, sub
                                        + " File is alreday in storage");
                                addDownlodedFileIntoArray(sub);
                            }

                        }
                        for (int j = 1; j <= 8; j++) {
                            if (mJsonObject.has(GlobalKeys.IMAGE_ + j)) {
                                String image_url = mJsonObject
                                        .getString(GlobalKeys.IMAGE_ + j);
                                String nonIntegerString = image_url.replaceAll(
                                        "[0-9]", "");
                                String sub = nonIntegerString.substring(
                                        nonIntegerString.lastIndexOf("/") + 1,
                                        nonIntegerString.lastIndexOf("."));
                                imageArrays.add(sub);
                                String filenameWithExt = sub + ".png";
                                if (!AppDefaultUtils
                                        .isFilePresent(filenameWithExt)) {
                                    new GetUserImageAPIHandler(mActivity,
                                            image_url,
                                            downloadImageResponse(sub), false,
                                            i);
                                } else {
                                    AppDefaultUtils.showLog(TAG, sub
                                            + " File is alreday in storage");
                                    addDownlodedFileIntoArray(sub);
                                }

                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
			/* Fail of API Response */
            AppDialogUtils.showMessageInfoWithOkButtonDialog(
                    mActivity,
                    WebserviceResponseHandler.getInstance().getResponseMessage(
                            response), null);
        }
    }

    /**
     * Check Trip API Listener
     */
    private WebAPIResponseListener downloadImageResponse(final String image_name) {
        WebAPIResponseListener mListener = new WebAPIResponseListener() {

            @Override
            public void onSuccessOfResponse(Object... arguments) {
                // TODO Auto-generated method stub
                if (arguments != null && arguments.length > 0
                        && arguments[0] != null
                        && !arguments[0].toString().isEmpty()) {
                    Bitmap bitmap = (Bitmap) arguments[0];
                    AppDefaultUtils.showLog(TAG, "bitmap : " + bitmap);
                    AppDefaultUtils.savePassengerBitmapToSDCard(bitmap,
                            image_name
					/* GlobalKeys.USER_IMAGE_NAME + "_" + (image_name) */);
                    addDownlodedFileIntoArray(image_name);

                }
            }

            @Override
            public void onFailOfResponse(Object... arguments) {
                // TODO Auto-generated method stub
            }
        };
        return mListener;
    }

    private void addDownlodedFileIntoArray(String file_name) {
        // imageArrays
        if (!mBitmapPathArray.contains(file_name)) {
            mBitmapPathArray.add(file_name);
        }
        if (dataCount == imageArrays.size()
                && (imageArrays.size() == mBitmapPathArray.size())) {
            ArrayList<String> sortedImageArray = new ArrayList<String>();
            for (String imageName : imageArrays) {
                for (String mPathImageName : mBitmapPathArray) {
                    if (mPathImageName.equalsIgnoreCase(imageName)) {
                        if (!sortedImageArray.contains(mPathImageName)) {
                            sortedImageArray.add(mPathImageName);
                        }
                    }
                }
            }
            mBitmapPathArray = new ArrayList<String>();

            if (dataCount == sortedImageArray.size()
                    && dataCount == imageArrays.size()) {
                AppHelper.catholicImagesArray = null;
                AppHelper.catholicImagesArray = new String[sortedImageArray
                        .size()];
                AppHelper.catholicImagesArray = sortedImageArray
                        .toArray(AppHelper.catholicImagesArray);

                AppHelper.catholicNameArray = null;
                AppHelper.catholicNameArray = new String[imageArrays.size()];
                AppHelper.catholicNameArray = imageArrays
                        .toArray(AppHelper.catholicNameArray);

                AppDefaultUtils.showVerboseLog(TAG,
                        "NOW APP IS USING ONLINE DATA");

                for (String string : AppHelper.catholicNameArray) {
                    AppDefaultUtils.showErrorLog(TAG, "NameArray :" + string);
                }
                AppDefaultUtils.showLog(TAG, "________________-");
                for (String string : AppHelper.catholicImagesArray) {
                    AppDefaultUtils.showErrorLog(TAG, "Image name :" + string);
                }
            }

        }
        // AddBitmapArrayFormat(mBitmapPathArray);
    }

    // @SuppressLint("DefaultLocale")
    // private void AddBitmapArrayFormat(ArrayList<String> mBitArray) {
    // if (AppHelper.catholicNameArray != null
    // && AppHelper.catholicNameArray.length == mBitmapPathArray
    // .size()) {
    // if (AppHelper.catholicNameArray != null) {
    // ArrayList<String> sortedImageArray = new ArrayList<>();
    // for (String nameOfImage : AppHelper.catholicNameArray) {
    // for (String path : mBitArray) {
    // String filename = path.substring(
    // path.lastIndexOf("/") + 1).replaceAll(".png",
    // "");
    // if (filename.equalsIgnoreCase(nameOfImage)) {
    // if (!sortedImageArray.contains(path)) {
    // sortedImageArray.add(path);
    // }
    //
    // }
    // }
    //
    // }
    // mBitmapPathArray = new ArrayList<>();
    // AppHelper.catholicImagesArray = new String[sortedImageArray
    // .size()];
    // AppHelper.catholicImagesArray = sortedImageArray
    // .toArray(AppHelper.catholicImagesArray);
    // }
    //
    // }
    //
    // }

    // /**
    // * Fill game data into array
    // *
    // * @param stockList
    // */
    // private void fillArrayWithData(ArrayList<String> stockList) {
    // AppHelper.catholicNameArray = new String[stockList.size()];
    // AppHelper.catholicNameArray = stockList
    // .toArray(AppHelper.catholicNameArray);
    //
    // }
}
