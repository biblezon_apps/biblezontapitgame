package com.biblezon.tapIt.model;

/**
 * letter bin model
 * 
 * @author Shruti
 * 
 */
public class SpellWordModel {
	String letter;
	boolean correct;
	boolean isEmpty;

	public boolean isEmpty() {
		return isEmpty;
	}

	public void setEmpty(boolean isEmpty) {
		this.isEmpty = isEmpty;
	}

	public String getLetter() {
		return letter;
	}

	public void setLetter(String letter) {
		this.letter = letter;
	}

	public boolean isCorrect() {
		return correct;
	}

	public void setCorrect(boolean correct) {
		this.correct = correct;
	}
}
