package com.biblezon.tapIt;

import java.util.ArrayList;

import android.app.Application;

public class KidsPuzzleApp extends Application {

	public static String whichGame;
	public static int movement = 0;
	public static ArrayList<String> gameList;

	public static void saveData(String whichGame, int movement,
			ArrayList<String> gameList) {
		KidsPuzzleApp.whichGame = whichGame;
		KidsPuzzleApp.movement = movement;
		KidsPuzzleApp.gameList = gameList;
	}

	public static int getMovement() {
		return movement;
	}

	public static ArrayList<String> getGameList() {
		return gameList;
	}

}
