package com.biblezon.tapIt.utils;

public interface OnMovementListener {
	public abstract void onMovement();
}
