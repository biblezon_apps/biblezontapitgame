package com.biblezon.tapIt.ihelper;

/**
 * Application Keys
 *
 * @author Anshuman
 */
public interface GlobalKeys {

    String BASE_URL = "http://biblezon.com/appapicms/webservices/";
    String MEMORYMATCHES = "memorymatches";

    /* API Keys */
    String SACRAMENTS_KEY = "sacraments_key";
    String ACTIVITY_KEY = "activity_key";
    String GET_IMAGE_KEY = "image_key";
    String USER_IMAGE_NAME = "img";
    String USER_IMAGE_FOLDER_NAME = "Sacraments_images";

    String RESPONSE_CODE = "replyCode";
    String RESPONSE_MESSAGE = "replyMsg";
    String SUCCESS = "SUCCESS";
    String DATA = "data";

    /* Response Activity Keys */
    String RESPONSE_ID = "id";
    String RESPONSE_TITLE = "title";
    String RESPONSE_DEFINITION = "definition";
    String RESPONSE_IMAGE = "image";

    /* Image Ids */
    String IMAGE_DEFAULT = "image_default";
    String IMAGE_ = "image_";

    /**
     * version check keys
     */
    String AUTO_UPDATE_URL = "http://biblezonadmin.com/biblezon/apk/checkversion.php?app=com.biblezon.tapIt&deviceId=";
    String VERSION_CHECK_KEY = "version_check";
    String MSG_SUCCESS = "success";
    String LATEST_VERSION = "latestVersion";
    String UPDATED_APP_URL = "appURI";
    String APK_NAME = "Biblezon_Games";

}
